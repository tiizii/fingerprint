package com.fingerprint.demo;

import com.fingerprint.demo.service.EncodingException;
import com.fingerprint.demo.service.FingerprintService;
import com.fingerprint.demo.service.NoFingerprintFoundException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FingerprintServiceIntegrationTest {

    @Autowired
    private FingerprintService fingerprintService;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private InputStream createInputStream(String text) {
        return new ByteArrayInputStream(text.getBytes());
    }

    @Test
    public void testEncode() throws EncodingException, IOException {
        String text = "valgusfoor";
        String name = "foo";
        String expectedText = "valgusẝᴏᴏr";

        byte[] result = fingerprintService.encode(createInputStream(text), name);
        Assert.assertTrue(new String(result).contains(expectedText));
    }

    @Test
    public void testShouldThrowsAnExceptionWhenTextContainsConfusables() throws EncodingException, IOException {
        String text = "valgusẝᴏᴏr";
        String name = "foo";

        exceptionRule.expect(EncodingException.class);
        exceptionRule.expectMessage("Fingerprinting not possible, text already contains confusables");
        fingerprintService.encode(createInputStream(text), name);
    }

    @Test
    public void testShouldThrowsAnExceptionWhenInputNameDoesNotFitToText() throws EncodingException, IOException {
        String text = "valgusfor";
        String name = "foo";

        exceptionRule.expect(EncodingException.class);
        exceptionRule.expectMessage("Fingerprinting not possible, ");
        fingerprintService.encode(createInputStream(text), name);
    }

    @Test
    public void testShouldThrowsAnExceptionWhenInputNameContainsUnconfusableLetter() throws EncodingException, IOException {
        String text = "valgus%for";
        String name = "%";

        exceptionRule.expect(EncodingException.class);
        exceptionRule.expectMessage("Can not use letter % in fingerprint.");
        fingerprintService.encode(createInputStream(text), name);
    }

    @Test
    public void testDecodeThrowsAnExceptionWhenTextDoNotContainExpectedConfusables() throws NoFingerprintFoundException, IOException {
        String text = "valgusfoor";

        exceptionRule.expect(NoFingerprintFoundException.class);
        exceptionRule.expectMessage("No fingerprint found");
        fingerprintService.decode(createInputStream(text));
    }

    @Test
    public void testDecode() throws NoFingerprintFoundException, IOException {
        String text = "valgusẝᴏᴏr";

        String ownerName = fingerprintService.decode(createInputStream(text));
        Assert.assertEquals(ownerName, "foo");
    }

}
