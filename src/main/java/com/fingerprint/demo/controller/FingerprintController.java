package com.fingerprint.demo.controller;

import com.fingerprint.demo.service.FingerprintService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;

@RestController
public class FingerprintController {

    private FingerprintService fingerprintService;

    public FingerprintController(FingerprintService fingerprintService) {
        this.fingerprintService = fingerprintService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/fingerprint", produces = "text/plain;charset=UTF-8")
    public byte[] fingerprint(InputStream text, @RequestParam("t") String message) throws Exception {
        return fingerprintService.encode(text, message);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/identify")
    public String indentify(InputStream text) throws Exception {
        return "book owner is: " + fingerprintService.decode(text);
    }
}
