package com.fingerprint.demo.service;

import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class FingerprintService {

    public byte[] encode(InputStream text, String name) throws EncodingException, IOException {
        Map<String, List<String>> confusables = readConfusables();
        StringBuilder encodedText = new StringBuilder("");

        try(BufferedReader reader = new BufferedReader(new InputStreamReader(text))) {
            String[] lettersInName = name.split("");
            String line;
            int nameLetterIndex = 0;

            while((line = reader.readLine())!=null) {
                findExistingConfucables(confusables, line);

                StringBuilder lineBuilder = new StringBuilder(line);
                int replacementIndex = 0;

                while (nameLetterIndex < lettersInName.length && replacementIndex != -1) {
                    replacementIndex = lineBuilder.toString().indexOf(lettersInName[nameLetterIndex], replacementIndex);

                    if(replacementIndex != -1) {
                        lineBuilder = lineBuilder.replace(replacementIndex, replacementIndex + 1, findReplacement(lettersInName[nameLetterIndex], confusables));

                        nameLetterIndex++;
                    }
                }
                encodedText.append(lineBuilder);
                encodedText.append('\n');
            }

            if(nameLetterIndex == 0 || nameLetterIndex < lettersInName.length) {
                throw new EncodingException("Fingerprinting not possible, name does not fit to text");
            }
        }

        return encodedText.toString().getBytes(StandardCharsets.UTF_8);
    }

    public String decode(InputStream text) throws NoFingerprintFoundException, IOException {
        Map<String, List<String>> confusables = readConfusables();
        StringBuilder fingerprint = new StringBuilder();
        String nonConfusable;
        String line;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(text, Charset.forName("UTF-8")))) {

            while ((line = reader.readLine()) != null) {
                String[] lettersInLine = line.split("");

                for (String letter : lettersInLine) {
                    nonConfusable = findAndReplaceConfusable(confusables, letter);

                    if (nonConfusable != null) {
                        fingerprint = fingerprint.append(nonConfusable);
                    }
                }
            }

        }

        if(fingerprint.length() == 0) {
            throw new NoFingerprintFoundException("No fingerprint found");
        }

        return fingerprint.toString();

    }

    private Map<String, List<String>> readConfusables() throws IOException {
        Map<String, List<String>> confusables;
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("./src/main/resources/confusables.txt")))) {

            confusables = br.lines()
                    .map(l -> l.split("\t"))
                    .collect(Collectors.toMap(l -> l[0], l -> Arrays.asList(l).subList(1 , l.length)));

        }
        return confusables;
    }

    private String findReplacement(String c, Map<String, List<String>> confusables) throws EncodingException {
        if(!confusables.containsKey(c)) {
            throw new EncodingException("Can not use letter " + c + " in fingerprint.");
        }

        return confusables.get(c).get(0);
    }

    private void findExistingConfucables (Map<String, List<String>> map, String line) throws EncodingException {
        String[] lettersInLine = line.split("");

        for (String letter : lettersInLine) {
            if(map.entrySet().stream().anyMatch(entry -> entry.getValue().contains(letter))) {
                throw new EncodingException("Fingerprinting not possible, text already contains confusables");
            }
        }
    }

    private String findAndReplaceConfusable(Map<String, List<String>> map, String letter) {
        return map.entrySet()
                .stream()
                .filter(entry -> entry.getValue().contains(letter))
                .map(Map.Entry::getKey)
                .findFirst().orElse(null);
    }
}
