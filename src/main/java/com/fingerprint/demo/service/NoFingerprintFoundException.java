package com.fingerprint.demo.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoFingerprintFoundException extends Exception {
    public NoFingerprintFoundException(String message) {
        super(message);
    }
}
