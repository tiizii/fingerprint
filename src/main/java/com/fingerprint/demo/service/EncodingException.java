package com.fingerprint.demo.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EncodingException extends Exception {
    public EncodingException(String message) {
        super(message);
    }
}
