# Demo for fingerprinting a text file

## Running the app

       ./gradlew bootRun

For continuous auto-restart with DevTools run in other terminal window the following command:

        ./gradlew classes --continuous

## Running the tests

        ./gradlew clean test --info

The HTML report can be found here:

        build/reports/tests/test/index.html

## Fingerprint a file

To input a file, run the following command from file location folder:

        curl -X POST -H 'Content-Type:text/plain' --data-binary @file.txt http://localhost:8080/fingerprint?t=name -o output.txt

Replace "file.txt" with the file name and t parameter with owner name

## To see the fingerprint of encoded file

Run the following command from file location folder:

        curl -X GET http://localhost:8080/identify --data-binary @output.txt

Replace "output.txt" with the encoded file name. Only use files encoded by this demo
